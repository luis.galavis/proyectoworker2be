const UserService = require('../services/userService');
const Auth        = require('../services/auth');
const Mailer      = require('../services/mailer');

const controller = {};
controller.receiveValidToken = async (req, res, next)=>{
    try {
        const token = req.params.token
        const data = await Auth.validateTokenReset(token)
        if(data.valid){
            return res.status(200).render('auth/resetPassword', {
                title: 'Reset Password',
                layout: 'layouts/blank',
                resetPasswordToken: data.resetPasswordToken,
                resetPasswordExpires: data.resetPasswordExpires,
                extractScripts: true,
                extractStyles: true
              });
        }else{
            return res.status(401).render('error/timeexpire', {
                title: 'Expire time',
                layout: 'layouts/blank',
              });
        }
    } catch (error) {
        return next(error)
    }
};

controller.validateEmail = async (req, res, next)=>{
    try {
        const email = req.body.email;
        const data = await Auth.generateResetToken(email)
        UserService.updateOne({email: email}, data, {new: true}).then((rsp)=>{
            const host = req.headers.host
            Mailer.mailResetPass(email, rsp.resetPasswordToken, host, 'Dash template').then((rsp)=>{
                return res.render('auth/success', {
                    title: 'Success',
                    layout: 'layouts/blank',
                    msg: "The mail has been sent successfully Check your inbox, or spam tray"
                  });
            });
        });
    } catch (error) {
        return next(error)
    }
};
controller.resetPass = (req, res, next) => {
    let resetPasswordToken = req.params.token;
    let updated = req.body;
    Auth.changePass(resetPasswordToken, updated).then((rsp)=>{
        return res.render('auth/success', {
            title: 'Success',
            layout: 'layouts/blank',
            msg: "The password for the account has been changed successfully",
        });
    }).catch((err)=>{
        return next(err)
    });  
};
controller.renderForgotPass = (req, res, next)=>{
    res.render('auth/forgotPassword', {
        title: 'Forgot Password',
        layout: 'layouts/blank',
        message: req.flash('ForgotMessage'),
      });
};
controller.logOut = (req, res, next)=>{
    req.logOut();
    req.session.destroy(function(err){
          if(err) return next(err)
        res.redirect('/login');
    });
};
controller.renderLogIn = (req, res, next)=>{
    return res.render('auth/login',{
        title: 'Login',
        layout: 'layouts/blank',
        message: req.flash('loginMessage'),
      })
};
controller.renderSignUp = (req, res, next)=>{
    res.render('auth/signup', { title: 'Sign Up', 
    layout: 'layouts/blank',
    message: req.flash('signupMessage'),
    });
};


module.exports = controller;