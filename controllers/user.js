/**********************************************/
/****************PACKAGES**********************/
/**********************************************/
//
/**********************************************/
/****************UTILS*************************/
/**********************************************/
//
/***********************************************/
/****************MODELS*************************/
/**********************************************/
// 
/***********************************************/
/****************SERVICES***********************/
/**********************************************/
const UserService = require('../services/userService')
/*************************************/
/*************** FUNCTIONS ***********/
/*************************************/
const controller = {}
//***************** API ******************* */
/**
* 
* @param {*} req 
* @param {*} res 
* @param {*} next 
*/
controller.saludo = (req, res, next) => {
    UserService.saludo().then(( data ) => {
        return res.send(data)
    }).catch(err => { 
        return next(err) 
    }) 
};
/****************** RENDER ************************* */
controller.saludoRender = async (req, res, next) => {
    const data = await UserService.saludo()
    res.render('user/saludo',{ 
        title: 'Express', 
        data: data,
    });
};
controller.myProfileRender = (req, res, next)=>{
    let user = req.user;
    res.render('user/profile',{ 
        title: 'My Profile', 
        user: user,
    });
};
/*****************END*****************/
module.exports = controller;
