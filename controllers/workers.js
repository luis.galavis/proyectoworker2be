'use strict'

const Worker = require('../models/worker')

const controller = {}
/**
* 
* @param {*} req 
* @param {*} res 
* @param {*} next 
*/

function getWorker (req, res) {
    let workerId = req.params.workerId
    
    Worker.findById(workerId, (err, worker) => {
        if (err) return res.status(500).send({message:`Error al realizar la petición... ${err}` })
        if (!worker) return res.status(404).send({message:'Error, no existen el trabajador'})

        res.status(200).send({ worker })
    })
}

function getWorkers (req, res) {
    let workerId = req.params.workerId
    
    Worker.find({}, ((err, workers) => {
        if (err) return res.status(500).send({message:`Error al realizar la petición... ${err}` })
        if (!workers) return res.status(404).send({message:'Error, no existe trabajadores'})

        res.send(200, {workers})
    }))
}

function saveWorker(req, res) {
    console.log('POST /worker') 
    console.log(req.body)

    let worker = new Worker()
    worker.fullname = req.body.fullname
    worker.workedhours = req.body.workedhours 
    worker.salary = req.body.salary

    worker.save((err, workerStored) => {
        if (err) res.status(500).send({message: `Error al salvar en la base de datos: ${err}`})

        res.status(200).send({worker: workerStored})
    })
}

function updateWorker (req, res) {
    let workerId = req.params.workerId
    let update = req.body

    Worker.findByIdAndUpdate(workerId, update, (err, workerUpdated) => {
        if (err) return res.status(500).send({message:`Error al actualizar el trabajador... ${err}` })

        res.status(200).send({worker : workerUpdated})
    })
}

function deleteWorker (req, res) {
    let workerId = req.params.workerId

    Worker.findById(workerId, (err, worker) => {
        if (err) return res.status(500).send({message:`Error al borrar el trabajador... ${err}` })

        worker.remove(err => {
            if (err) return res.status(500).send({message:`Error al borrar el trabajador... ${err}` })

            res.status(200).send({message: 'el trabajador ha sido eliminado'})
        })
    })
}

/* RENDERS */

function addWorker ( req, res) {
    let user = req.user;
    res.render('worker/addworker',{ 
        title: 'Add new Worker', 
        user: user,
    });
};

module.exports = {
    getWorker,
    getWorkers,
    saveWorker,
    updateWorker,
    deleteWorker,
    addWorker,
}