'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const WorkerSchema = Schema ({
    fullname: String,
    workedhours: Number,
    salary: Number
})

module.exports = mongoose.model('Worker', WorkerSchema)