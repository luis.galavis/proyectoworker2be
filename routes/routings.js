module.exports = {
    ApiUserRouter: require('./user/apiRoutes'),
    UserRouter: require('./user/interfaceRoutes'),
    IndexRouter: require('./index'),
    ApiWorkerRouter: require('./worker/apiRoutes'),
    WorkerRouter: require('./worker/interfaceRoutes'),
}