/**********************************************/
/****************PACKAGES**********************/
/**********************************************/
const express = require('express')
const router = express.Router();
const Mid = require('../../middlewares/index');
/**********************************************/
/****************CONTROLLER********************/
/**********************************************/
const UserController = require('../../controllers/user')
/*************************************/
/*************** FUNCTIONS ***********/
/*************************************/
router.get('/saludo', Mid.isLoggedIn ,UserController.saludoRender);
router.get('/profile', Mid.isLoggedIn, UserController.myProfileRender);
/*****************END*****************/
module.exports = router;
