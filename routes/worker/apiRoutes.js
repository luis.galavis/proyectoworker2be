'use strict'

const express = require('express')
const workerCtrl = require('../../controllers/workers')
const api = express.Router()

api.get('/', workerCtrl.getWorkers)

api.get('/:workerId',  workerCtrl.getWorker)

api.post('/', workerCtrl.saveWorker)

api.put('/:workerId', workerCtrl.updateWorker)

api.delete('/:workerId', workerCtrl.deleteWorker)

module.exports = api 