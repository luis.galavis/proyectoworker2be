/**********************************************/
/****************PACKAGES**********************/
/**********************************************/
const express = require('express')
const router = express.Router();
const Mid = require('../../middlewares/index');
/**********************************************/
/****************CONTROLLER********************/
/**********************************************/
const WorkerController = require('../../controllers/workers')
/*************************************/
/*************** FUNCTIONS ***********/
/*************************************/

router.get('/addworker', Mid.isLoggedIn ,WorkerController.addWorker);
/*****************END*****************/
module.exports = router; 



