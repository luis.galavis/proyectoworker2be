const User = require('../models/user');
const moment = require('moment');
const bcrypt   = require('bcrypt');
const service = {}

/**
 * 
 * @param {String} id - token for reset password 
 * @returns boolean
 */
service.validateTokenReset = (id)=>{
    return new Promise((resolve, reject)=>{
        User.findOne({resetPasswordToken: id}).then((user)=> {
            if (!user) {
                const err = new Error('Not found')
                return reject(err)
            }else{
                if (moment().isBefore(moment(user.resetPasswordExpires))) {
                    
                   const data = {
                       valid : true,
                       resetPasswordToken: id,
                       resetPasswordExpires: moment(user.resetPasswordExpires),
                   }
                   return resolve(data)
                  } else {
                     const data = {
                         valid : false,
                         resetPasswordToken: null,
                         resetPasswordExpires: null,
                     }
                     return resolve(data)
                 }
            }
          }).catch((err)=> {return reject(err)})
    });
   
};
/**
 * 
 * @param {String} email 
 * @returns 
 */
service.generateResetToken = (email)=>{
    return new Promise((resolve, reject)=>{
        User.findOne({email: email}).then((user)=> {
            if (!user) {
                const err = new Error('Not found')
                return reject(err)
            }
            const resetPasswordToken = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            let resetPasswordExpires = moment().add(5, 'minutes').format(); // 10 minutes
            return resolve({
                resetPasswordToken   : resetPasswordToken,
                resetPasswordExpires : resetPasswordExpires
            });
          }).catch((err)=>{
              return reject(err)
          })
    })
  
};
/**
 * 
 * @param {String} resetPasswordToken 
 * @param {Object} updated 
 * @returns success
 */
service.changePass = (resetPasswordToken, updated)=>{
    return new Promise((resolve, reject)=>{  
      if(updated.password != updated.confirm){
            const err = new Error('passwords do not match') 
            return reject(err)
      }else{
        updated.password = bcrypt.hashSync(updated.password, bcrypt.genSaltSync(8), null);
        User.findOneAndUpdate({
            resetPasswordToken: resetPasswordToken
          }, updated,{}).then((user)=> {
              return resolve('success')
          }).catch((err)=>{
              return next(err)
          });
      }
    });
}
module.exports = service