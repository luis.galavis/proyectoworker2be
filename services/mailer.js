const nodeMailer  =  require('nodemailer');
const transport   =  require('../config/transport');
const transporter =  nodeMailer.createTransport(transport);
const mailTest    =  process.env.EMAIL_DEV;
const ENV         =   process.env.NODE_ENV;

const mailer = {}

/**
 * 
 * @param {String} from -  The email address of the sender.
 * @param {String} to - Comma separated list or an array of recipients email addresses that will appear on the To: field
 * @param {String} bcc - Comma separated list or an array of recipients email addresses that will appear on the Bcc: field
 * @param {String} priority - priority email ‘high’, ‘normal’ (default) or ‘low’.
 * @param {String} subject - The subject of the email
 * @param {String} msg - text or HTML message, this will be inserted into an html template inside email 
 * @param {Array} attachment - An array of attachment objects. You can use an empty array in case no attachment
 * @description Send emails from gmail
 */
mailer.SendEmail = (from, to, bcc, priority, subject, msg, attachment) => {
    mailOptions = {
      from: from,
      to:  ENV == 'production' ? to : mailTest,
      bcc: bcc,
      priority: priority,
      subject: ENV == 'production' ? subject : `${subject} - [DEV]`,
      attachments: attachment,
      html: `<!DOCTYPE html>
      <html> 
       <head> 
       <meta charset='utf-8'>
        <title></title> 
        </head> 
        <body> 
            <div style='font-family: monospace;'>
            <div style='padding: 10px; background: #08090a; border-bottom: 1px solid rgba(0, 40, 100, 0.12); text-align: center;'> 
              <img src='https://pngimg.com/uploads/amazon/amazon_PNG11.png' alt='logo-cata1og' height='55px' style='padding: 10px 0 2px 0;'> </div> <div style='background-color: rgb(245, 247, 251); padding: 10px;'> 
              <p style='text-align: center'>${msg}</p>
            </div>
            </div>
            <footer style='font-family: monospace; background: #08090a; border-top: 1px solid rgba(0, 40, 100, 0.12); font-size: 0.875rem; padding: 1.25rem 0; color: #9aa0ac;'>
              <div style='text-align: center'> 
                <span>Copyright © 2019
                  <a href='https://2becommerce.com' style='color:#f69b3c;'>2b eCommerce</a> All rights reserved.
                </span> 
              </div> 
            </footer> 
        </body>
      </html>`
    }
    transporter.sendMail(mailOptions, function(error, info){
      if (error) throw new Error('Error mailer')  
    }); 
    
  };
/**
 * 
 * @param {String} email The email address of the sender.
 * @param {String} resetPasswordToken token
 * @param {String} host string host
 * @param {String} projectName - project name header email 
 * @param {Strng} color - color header email
 */
  mailer.mailResetPass = (email, resetPasswordToken, host, projectName)=>{
      return new Promise((resolve, reject)=>{
        let mailOptions = {
            to: email,
            subject: `Password Reset [${projectName}]`,
            html:   `<!DOCTYPE html>
            <html> 
             <head> 
             <meta charset='utf-8'>
              <title></title> 
              </head> 
              <body> 
              <table style="width: 650px; margin: 0 auto; background-color: #fff; border-radius: 8px">
            <tbody>
              <tr>
                <td style="padding: 30px"> 
                  <h3 style="font-weight: 600">Password Reset</h3>
                  <p>you forgot your password for ${projectName}. If this is true, click below to reset your password.</p>
                  <p style="text-align: center"><a href="http://${host}/reset/${resetPasswordToken}"
                   style=" cursor: pointer; text-decoration: none; padding: 10px; background-color: #4466f2; color: #fff; display: inline-block; border-radius: 4px">Reset Password</a></p>
                  <p>If you have remember your password you can safely ignore his email.</p>
                  <p style=" margin-bottom: .25rem !important;">If you have trouble clicking the "Reset
                  password ", copy and paste the following URL in your web browser</p>
              <div>http://${host}/reset/${resetPasswordToken}</div>
                  <p>Good luck! Hope it works.</p>
                  <p style="margin-bottom: 0">
                    Regards,<br>2B Developers</p>
                </td>
              </tr>
            </tbody>
          </table>
          </body>
            </html>`
          
          };
          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
             return reject(error)
            }else{
                return resolve('success')
            }
          });
      })
  
  };

  

  module.exports = mailer;

