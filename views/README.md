## VIEWS

- Esta carpeta se utiliza para el almacenamiento de los archivos que contienen la estructura visual de los componentes, separado por componentes
- Formato: camelCase

- /main
    - index.ejs

- /auth
    - login.ejs
    - forgotPassword.ejs
    - resetPassword.ejs
    - signup.ejs

- /error
    - 401.ejs
    - 404.ejs
    - 502.ejs
    - 504.ejs

- /layouts
    - head.ejs
    - footer.ejs
    - nav.ejs
    - script.ejs
    - style.ejs
